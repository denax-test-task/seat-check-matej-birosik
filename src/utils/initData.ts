interface ISeats {
    [key: string]: boolean;
}

export interface IReservedSeats {
    id: string;
    row: number;
    seat: number;
}

interface IInitData {
    rows: number;
    seatsInRow: number;
    seats: ISeats;
    reservedSeats: Array<IReservedSeats>;
    seatIndex: number
}

const getInitData = (): IInitData => {              /* This will basicly create in the end of fuction object of booleans for checkbox state managment and return object for initial setup*/ 
    const rows = 10;
    const seatsInRow = 20;
    const reservedSeats = [{
        id: 'seat21',
        row: 2,
        seat: 1
    }, {
        id: 'seat22',
        row: 2,
        seat: 2
    }, {
        id: 'seat23',
        row: 2,
        seat: 3
    }];
    const seats: any = {};

    for(let i = 1; i <= rows * seatsInRow; ++i) {
        const seatID = `seat${i}`;
        seats[seatID] = typeof reservedSeats.find(reservedSeat => reservedSeat.id === seatID) === 'object';
    }
    
    return {
        rows,
        seatsInRow,
        ...seats,
        seats,
        reservedSeats,
        seatIndex: 0
    }
}


export const initData = getInitData();
