import { theme } from '../theme/';
import SeatPicker from './SeatPicker';

import { 
    CssBaseline, 
    ThemeProvider, 
    createStyles,
    withStyles 
} from '@material-ui/core';

interface IAppMaterialUiProps {
    classes: {
      [key: string]: string;
    };
}

const styles = () => createStyles({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%',
        minHeight: '100vh',
        background: '#4AA8FF',
    },
    appContent: {
        maxWidth: 1500,
        minWidth: 1100,
        padding: 20,
        overflow: 'hidden',
        background: 'white',
        borderRadius: '25px'
    }
});

const App: React.FC<IAppMaterialUiProps> = ({
    classes
}) => {
    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <CssBaseline />
                <div className={classes.appContent}>
                    <SeatPicker />
                </div>
            </div>
        </ThemeProvider>
    );
};

export default withStyles(styles)(App);