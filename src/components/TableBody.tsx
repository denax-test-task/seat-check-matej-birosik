import { 
    useState,
} from 'react';
import classNames from 'classnames';
import ReservedSeats from './ReservedSeats';
import { initData } from '../utils/initData';

import { 
    createStyles,
    withStyles,
    Grid,
    Checkbox,
    Typography
} from '@material-ui/core';

interface IClasses {
    [key: string]: string
}

interface ISeats {
    row: number;
    seat: number;
}

interface ITableBodyProps {
    classes: IClasses;
};

const styles = () => createStyles({
    root: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    cell: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 30,
        height: 30,
    },
    cellBig: {
        width: 60,
        justifyContent: 'start',
    },
});

const TableBody: React.FC<ITableBodyProps> = ({
    classes
}) => {

    const { 
        rows,
        seatsInRow,
        seats,
        reservedSeats, 
    } = initData;

    let {
        seatIndex
    } = initData;

    const [seatState, setSeatState] = useState(seats);
    const [seatResState, setSeatResState] = useState(reservedSeats)

    const deleteReservedSeat = (seatID: string) => (e: React.MouseEvent<HTMLButtonElement>) => {
        setSeatResState([...seatResState.filter(reservedSeat => reservedSeat.id !== seatID)])
        setSeatState({ ...seatState, [seatID]: false });
    }

    const onClickCheck = (param: ISeats) => (e: React.ChangeEvent<HTMLInputElement>) => {       // on checkbox click will check or uncheck specific checkbox
        const seatID = e.target.name;
        const { row, seat } = param;

        if (e.target.checked) {
            setSeatResState([...seatResState, {
                id: seatID, row, seat
            }])
        } else {
            setSeatResState([...seatResState.filter(reservedSeat => reservedSeat.id !== seatID)])
        }

        setSeatState({ ...seatState, [seatID]: e.target.checked });
    };

    const getInput = (param: ISeats) => {       // returning checkbox element || header names || left row names in table
        const {row, seat} = param;
        let name = '';

        if (seat === 0 && row > 0) {
            name = `Rad ${row}`
        } else if (row === 0 && seat > 0) {
            name = (seat).toString();
        } else {
            if (row === 0) return;

            seatIndex++;
            const seatID = `seat${seatIndex}`;

            return (
                <Checkbox
                    style={{transform: "scale(0.7)"}}
                    checked={seatState[seatID]}
                    onChange={onClickCheck(param)}
                    name={seatID}
                    color="primary"
                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                />
            )
        }
    
        return (
            <Typography variant="button" className={classes.textCount}>
                {name}
            </Typography>
        )
    }

    const createTableBody = (classes: IClasses, row: number) => {
        return (
            <Grid item>
                <Grid container direction="row">
                    {
                        [...Array(seatsInRow + 1)].map((_, seat) => {
                            return (
                                <Grid key={seat} item>
                                    <div className={classNames(classes.cell, seat === 0 && classes.cellBig)}>
                                        {getInput({
                                            row, 
                                            seat
                                        })}
                                    </div>
                            </Grid>
                        )})}
                </Grid>
            </Grid>
        )
    }

    return (
        <div className={classes.root}>
            <div>
                {   
                    [...Array(rows + 1)].map((_, row) => {
                        return (
                            <Grid key={row} container direction="column">
                                {createTableBody(classes, row)}
                            </Grid>
                        )
                    })
                }
            </div>
            <ReservedSeats seatResState={seatResState} deleteReservedSeat={deleteReservedSeat} />
            <Typography variant="button">
                {`Počet vybraných miest: ${seatResState.length}`}
            </Typography>
        </div> 
    )
}

export default withStyles(styles)(TableBody);