import TableBody from './TableBody';

import { 
    createStyles,
    withStyles
} from '@material-ui/core';

const styles = () => createStyles({
});

const SeatPicker: React.FC = () => {
    return (
        <div>
            <TableBody />
        </div>
    )
}

export default withStyles(styles)(SeatPicker);