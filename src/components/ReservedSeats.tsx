import { IReservedSeats } from '../utils/initData';
import classNames from 'classnames';

import { 
    createStyles,
    withStyles, 
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    Typography,
    IconButton
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

interface IClasses {
    [key: string]: string
}

interface IChoosenSeatsProps {
    classes: IClasses;
    seatResState: Array<IReservedSeats>;
    deleteReservedSeat: (seatID: string) => (e: React.MouseEvent<HTMLButtonElement>) => void;  // // passed function from parent component this function basicly deleting  value from array and also unselect checkbox
};


const styles = () => createStyles({
    root: {
        width: 200,
        marginLeft: 10,
        paddingTop: 3,
        maxHeight: 330
    },
    padding: {
        padding: 0
    },
    margin: {
        marginTop: 5
    },
    listContainer: {
        marginTop: 5,
        maxHeight: 290,
        overflow: 'auto'
    }
});

const ReservedSeats: React.FC<IChoosenSeatsProps> = ({
    classes,
    seatResState,
    deleteReservedSeat
}) => {
    
    return (
        <div className={classes.root}>
            <Typography variant="button">
                Vybrané miesta:
            </Typography>
            <div className={classes.listContainer}>
                {
                    seatResState.map((seat, index) => {

                        return (
                            <List key={index} className={classNames(classes.padding, classes.margin)}>
                                <ListItem className={classes.padding}>
                                <ListItemText
                                    primary={`Rad ${seat.row}, Sedadlo ${seat.seat}`}
                                    disableTypography
                                />
                                <ListItemSecondaryAction>
                                <IconButton 
                                    onClick={deleteReservedSeat(seat.id)}
                                    aria-label="delete" 
                                    size="small"> 
                                    <DeleteIcon fontSize="small" />
                                </IconButton>
                                </ListItemSecondaryAction>
                                </ListItem>
                            </List>
                    )})}
            </div>
        </div>
    )
}

export default withStyles(styles)(ReservedSeats);